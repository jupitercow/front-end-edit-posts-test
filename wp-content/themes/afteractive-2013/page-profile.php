<?php
/**
 * Redirect non-logged in users to login
 */
if (! is_user_logged_in() )
{
	wp_redirect( wp_login_url( get_permalink() ) );
	die;
}

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
							<?php the_post_thumbnail(); ?>
						</div>
						<?php endif; ?>

						<h1 class="entry-title"><?php the_title(); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php the_content(); ?>

						<?php $links = acf_frontend_edit_posts::edit_link('post_type=store&echo=0'); ?>
						<?php if ( $links ) : ?>
							<h3>Edit your store</h3>
							<?php echo $links; ?>
						<?php endif; ?>

						<?php $links = acf_frontend_edit_posts::edit_link('post_type=product&echo=0'); ?>
						<?php if ( $links ) : ?>
							<h3>Edit your products</h3>
							<?php echo $links; ?>
						<?php endif; ?>

						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div><!-- .entry-content -->

					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-meta -->
				</article><!-- #post -->

				<?php comments_template(); ?>
			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>