<?php

/**
 * Add custom post types for users to edit
 *
 * This is using a basic wrapper function from our class, so that these post types are associated with our edit posts setup.
 */
if ( class_exists('acf_frontend_edit_posts') ) :
acf_frontend_edit_posts::register_post_type( 'store', array(
	'labels'              => array(
		'name'               => 'Stores',
		'singular_name'      => 'Store',
		'all_items'          => 'All Stores',
		'add_new'            => 'Add New',
		'add_new_item'       => 'Add New Store',
		'edit'               => 'Edit',
		'edit_item'          => 'Edit Stores',
		'new_item'           => 'New Store',
		'view_item'          => 'View Store',
		'search_items'       => 'Search Stores', 
		'not_found'          => 'Nothing found in the Database.', 
		'not_found_in_trash' => 'Nothing found in Trash',
		'parent_item_colon'  => ''
	),
	'description'         => "Holds info for user's store",
	'public'              => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'show_ui'             => true,
	'query_var'           => true,
	'rewrite'	          => array( 'slug' => 'store', 'with_front' => false ),
	'has_archive'         => 'stores',
	'capability_type'     => 'post',
	'hierarchical'        => false,
	'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'revisions' ),
	'edit_page'           => 'edit-store'
) );
acf_frontend_edit_posts::register_post_type( 'product', array(
	'labels'              => array(
		'name'               => 'Products',
		'singular_name'      => 'Product',
		'all_items'          => 'All Products',
		'add_new'            => 'Add New',
		'add_new_item'       => 'Add New Product',
		'edit'               => 'Edit',
		'edit_item'          => 'Edit Products',
		'new_item'           => 'New Product',
		'view_item'          => 'View Product',
		'search_items'       => 'Search Products', 
		'not_found'          => 'Nothing found in the Database.', 
		'not_found_in_trash' => 'Nothing found in Trash',
		'parent_item_colon'  => ''
	),
	'description'         => "Holds info for user's products",
	'public'              => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'show_ui'             => true,
	'query_var'           => true,
	'rewrite'	          => array( 'slug' => 'product', 'with_front' => false ),
	'has_archive'         => 'products',
	'capability_type'     => 'post',
	'hierarchical'        => false,
	'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'revisions' ),
	'edit_page'           => 'edit-product'
) );
endif;

/**
 * Remove automatic title and content in the frontend edit post.
 * /
add_filter( 'acf/frontend_edit_posts/title_content', '__return_false' );

/**
 * Redirect users away from wp-admin area. Currently non-contributors.
 *
 * @return 	void
 */
add_action( 'admin_init', 'custom_redirect_fromadmin', 1 );
	function custom_redirect_fromadmin()
	{
		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return;
		if ( defined('DOING_AJAX') && DOING_AJAX ) return;
		if ( $_SERVER['PHP_SELF'] == '/wp-admin/async-upload.php' ) return;

		if ( current_user_can('delete_posts') ) return;

		$redirect = home_url('/');

		$page = get_page_by_path('profile');
		if ( is_object($page) ) $redirect = get_permalink($page->ID);

		wp_safe_redirect( $redirect );
		die;
	}

/**
 * Remove admin bar for users. Currently for non-contributor.
 *
 * @return 	bool True if user has specified capability, false if not.
 */
add_filter( 'show_admin_bar', 'custom_remove_adminbar' );
	function custom_remove_adminbar( $content )
	{
		return ( current_user_can('delete_posts') ) ? $content : false;
	}

/**
 * Remove styles that are getting in the way
 *
 * @return 	void
 */
add_action( 'wp_print_styles', 'remove_acf_styles' );
	function remove_acf_styles()
	{
		// This one is KEY, removes admin styles that get added by default
		if (! is_admin() ) wp_deregister_style( 'wp-admin' );

		// Use this to deregister styles for frontend notifications plugin
		#wp_dequeue_style('frontend_notifications');

		/**
		 * Use this to remove a bunch of other defaults to ease styling, if it is too much, they can easily be added back in.
		 * This does not include the many separate "input" styles that are loaded by specific fields.
		 * /
		$styles = array(
			'acf',
			'acf-field-group',
			'acf-global',
			'acf-input',
			'acf-datepicker'
		);
		foreach( $styles as $v )
		{
			wp_dequeue_style( $v );
		}
		/**/
	}

/**
 * Turn off auto adding of post type fields in profile
 */
add_filter( 'acf/frontend_edit_posts/profile_post_types', '__return_false' );

/**
 * Change post_type field meta name for users. This changes it globally for all post types used by plugin.
 *
 * @param	string $post_type The post type name, can be modified or replaced.
 * @return	string The modified post type name that matches your field.
 */
add_filter( 'acf/frontend_edit_posts/post_type_meta_name', 'custom_post_type_meta_name' );
	function custom_post_type_meta_name( $post_type )
	{
		return $post_type . '_post_id';
	}

/**
 * Change post_type field meta name for users. This changes it for a specific post_type used by plugin. "store" in this case.
 *
 * @param	string $post_type The post type name, can be modified or replaced, if modified globally, this is the modified version.
 * @param	string $orig The original post type name, unmodified by global changes.
 * @return	string The modified post type name that matches your field.
 * /
add_filter( 'acf/frontend_edit_posts/post_type_meta_name/type=store', 'custom_post_type_meta_name_store', 10, 2 );
	function custom_post_type_meta_name_store( $post_type, $orig )
	{
		return $orig . '_post_id';
	}

/**
 * Redirect logged in users from login page to profile
 *
 * @return 	string Profile page page slug.
 */
add_filter( 'customize_login/logged_in_redirect', 'custom_logged_in_redirect' );
	function custom_logged_in_redirect()
	{
		return 'profile';
	}

/**
 * Change "Content" label on post_content fields to "Description"
 *
 * @return 	string New post_content label.
 */
add_filter( 'acf/edit_title_content/content/title', 'custom_change_post_content_label' );
	function custom_change_post_content_label()
	{
		return 'Description';
	}

/**
 * Change admin email addresses that notifications are sent to
 *
 * @return 	array Admin email address(es) to send notifications to
 * /
add_filter( 'acf/notify_admin/email/addresses', 'custom_notify_admin_email_addresses' );
	function custom_notify_admin_email_addresses( $admin_emails )
	{
		$admin_emails = array(
			get_option('admin_email')
		);

		return $admin_emails;
	}

/**
 * Customize email subject
 *
 * @return 	string Modified email subject
 * /
add_filter( 'acf/notify_admin/email/message', 'custom_notify_admin_email_subject', 10, 2 );
	function custom_notify_admin_email_subject( $subject, $post_id )
	{
		// get the current user
		$current_user = wp_get_current_user();

		$full_name = ( $current_user->first_name || $current_user->last_name ? $current_user->first_name . ' ' . $current_user->last_name : '');
		$subject = esc_html(get_option('blogname')) . ': Post Edited' . ($full_name ? ' by ' . esc_html($full_name) : '');

		return $subject;
	}

/**
 * Customize admin email notification message, using an html template here, because the customize emails plugin allows it. 
 * Default for WP is plaintext (also default for notifications). Keep that in mind if not using the customize emails plugin.
 *
 * @return 	string Modified email message
 */
add_filter( 'acf/notify_admin/email/message', 'custom_notify_admin_email_message', 10, 2 );
	function custom_notify_admin_email_message( $message, $post_id )
	{
		// get the post
		$post = get_post($post_id);
		$post_type = $post->post_type;

		// get the current user
		$current_user = wp_get_current_user();

		$full_name = ( $current_user->first_name || $current_user->last_name ? $current_user->first_name . ' ' . $current_user->last_name : '');

		ob_start();
		?>
		<p>A <?php echo $post_type; ?> was recently edited on <a href="<?php echo home_url('/'); ?>"><?php echo home_url(); ?></a></p>
		<p>Date: <?php echo date_i18n(get_option('date_format')); ?>, <?php echo date_i18n(get_option('time_format')); ?><br />
			Post title: <?php echo esc_html(get_the_title($post_id)); ?><br />
			User who edited:<br />
				<ul>
					<li>Name: <?php echo esc_html($full_name); ?></li>
					<li>Email: <?php echo esc_html($current_user->user_email); ?></li>
					<li>Username: <?php echo esc_html($current_user->user_login); ?></li>
				</ul>
			View post: <a href="<?php echo get_permalink($post_id); ?>"><?php echo get_permalink($post_id); ?></a><br />
			Edit post: <a href="<?php echo acf_notify_admin::get_edit_post_link($post_id, ''); ?>"><?php echo acf_notify_admin::get_edit_post_link($post_id); ?></a>
		</p>
		<?php
		$message = ob_get_clean();

		return $message;
	}

/**
 * Add supported URL query variables and their corresponding messages for notifications
 *
 * @return 	array Modified $queries from plugin with any new or changed queries that we need for our application
 */
add_filter( 'frontend_notifications/queries', 'custom_add_query_notifications' );
	function custom_add_query_notifications( $queries )
	{
		$queries[] = array(
			'key' => 'update',
			'value' => 'true',
			'message' => "Updated.",
			'args' => 'fade=true'
		);
		$queries[] = array(
			'key' => 'update',
			'value' => 'failed',
			'message' => "Unable to update.",
			'args' => 'fade=true&error=true'
		);
		$queries[] = array(
			'key' => 'update',
			'value' => 'published',
			'message' => "Published!",
			'args' => 'fade=10000'
		);
		return $queries;
	}

/**
 * Remove notifications scripts and styles and then add them into ACF, so they are added to all acf form pages instead of all pages.
 */
add_filter( 'frontend_notifications/enqueue_scripts', '__return_false' );
if ( class_exists('frontend_notifications') ) add_action( 'acf/input/admin_enqueue_scripts', array('frontend_notifications', 'enqueue_scripts') );