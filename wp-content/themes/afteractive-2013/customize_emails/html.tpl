<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html;UTF-8" />

    <title>%blogname%</title>

</head>

<body style="margin: 0px; background-color: #F4F3F4; font-family: Helvetica, Arial, sans-serif; font-size:12px;" text="#444444" bgcolor="#F4F3F4" link="#21759B" alink="#21759B" vlink="#21759B" marginheight="0" topmargin="0" marginwidth="0" leftmargin="0">
    <table width="100%" border="0" cellspacing="0" cellpadding="15" bgcolor="#F4F3F4">
        <tbody>
            <tr>
                <td>

<center>
<table class="contenttable" width="550" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF">
    <tbody>
        <tr>
            <td align="left">
                <div style="border: solid 1px #d9d9d9;">

					<table id="header" style="line-height: 1.6; font-size: 12px; font-family: Helvetica, Arial, sans-serif; border: solid 1px #FFFFFF; color: #444;" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
					    <tbody>
					        <tr><td style="color: #ffffff;" colspan="2" valign="bottom" height="30"> &nbsp; </td></tr>
					        <tr>
					            <td style="line-height: 32px; padding-left: 30px;" colspan="2" valign="baseline">
					            	<span style="font-size: 32px;"><a style="text-decoration: none;" href="%siteurl%" target="_blank">%blogname%</a></span>
					            </td>
					        </tr>
					    </tbody>
					</table>

					<table id="content" style="margin-top: 15px; margin-right: 30px; margin-left: 30px; color: #444; line-height: 1.6; font-size: 12px; font-family: Arial, sans-serif;" width="490" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
					    <tbody>
					        <tr>
					            <td style="border-top: solid 1px #d9d9d9;" colspan="2">
					                <div style="padding: 15px 0;">
					                    %content%
					                </div>
					            </td>
					        </tr>
					    </tbody>
					</table>

					<table id="footer" style="line-height: 1.5; font-size: 12px; font-family: Arial, sans-serif; margin-right: 30px; margin-left: 30px;" width="490" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
					    <tbody>
					        <tr style="font-size: 11px; color: #999999;">
					            <td style="border-top: solid 1px #d9d9d9;" colspan="2">
					                <div>
					                	%footer%
					                </div>
					            </td>
					        </tr>
					        <tr><td style="color: #ffffff;" colspan="2" height="15"> &nbsp; </td></tr>
					    </tbody>
					</table>

                </div>
            </td>
        </tr>
    </tbody>
</table>
</center>

                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
