<?php
/*
Plugin Name: Frontend Notifications
Plugin URI: http://bitbucket.org/jupitercow/frontend-notifications
Description: Supports notifications on the front end based off of query variables. Notifications can also be added manually.
Version: 0.1
Author: Jake Snyder
Author URI: http://Jupitercow.com/
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

------------------------------------------------------------------------
Copyright 2013 Jupitercow, Inc.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

if (! class_exists('frontend_notifications') ) :

add_action( 'init', array('frontend_notifications', 'init') );

class frontend_notifications
{
	/**
	 * Class prefix
	 *
	 * @since 	0.1
	 * @var 	string
	 */
	public static $prefix = __CLASS__;

	/**
	 * The default settings
	 *
	 * @since 	0.1
	 * @var 	string
	 */
	public static $version = '0.1';

	/**
	 * Holds the current notifications
	 *
	 * @since 	0.1
	 * @var 	array
	 */
	private $notifications = array();

	/**
	 * Holds the supported queries and their messages
	 *
	 * @since 	0.1
	 * @var 	array
	 */
	private $queries = array();

	/**
	 * Holds the main instance
	 *
	 * @since 	0.1
	 * @var 	string
	 */
	private static $_instance = NULL;

	/**
	 * Get instance of class
	 *
	 * Sets up the base, global istance of the class to be used when no specific instances have been created.
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	object The global instance of this class.
	 */
	public static function get_instance()
	{
		return ( NULL === self::$_instance ) ? self::$_instance == new self : self::$_instance;
	}

	/**
	 * Class construct
	 *
	 * This class can actually be instantiated to create specific notification streams outside of global.
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	function __construct()
	{
		// Add an action to show single notifications
		add_action( self::$prefix . '/show', array($this, 'show_notifications') );

		// Add an action to show single notifications
		add_action( self::$prefix . '/add', array($this, 'add_notification'), 10, 2 );
	}

	/**
	 * Initialize the Class
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function init()
	{
		// Make sure an instance is created.
		self::get_instance();

		// Dismiss notification
		add_action( 'wp_ajax_' . self::$prefix . '_dismiss', array(__CLASS__, 'dismiss') );

		// enqueue scripts and styles
		if ( apply_filters( self::$prefix . '/enqueue_scripts', true ) )
			add_action( 'wp_enqueue_scripts', array(__CLASS__, 'enqueue_scripts') );
	}

	/**
	 * Dismiss persistant notifications permanently
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function dismiss()
	{
		if ( empty($_POST['event']) ) return false;

		$current_user = wp_get_current_user();
		update_user_meta( $current_user->ID, self::$prefix . '_' . esc_sql($_POST['event']) . '_dismissed', current_time('timestamp') );
	}

	/**
	 * Add queries and messages to the instance
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @param	string $args Arguments for a new notification(s). The query key/value for request, message for notification, and notification arguments.
	 * @return	void
	 */
	public function add_query( $args )
	{
		$defaults = array(
			'key'     => false,
			'value'   => false,
			'message' => false,
			'args'    => false
		);
		$args = wp_parse_args( $args, $defaults );

		$this->queries[] = $args;
	}

	/**
	 * Get the instance queries and messages, add any global queries and messages
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	array All of the queries and messages for this instance and globally
	 */
	public function get_queries()
	{
		return apply_filters( self::$prefix . '/queries', $this->queries );
	}

	/**
	 * Process query vars into notifications
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public function query_notifications()
	{
		$output = array();

		if ( $queries = $this->get_queries() )
		{
			foreach ( $queries as $query )
			{
				if (! empty($_REQUEST[$query['key']]) && $query['value'] == $_REQUEST[$query['key']] )
				{
					$output[] = array(
						'message' => $query['message'],
						'args'    => $query['args']
					);
				}
			}
		}

		return $output;
	}

	/**
	 * Use this to add a notification to the notification area
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @param	array|string $notifications The actual text of the notification(s) to add.
	 * @param	array|string $args A collection of arguments to customize the notification. 'dismiss', adds a close button. 'event' applies an event to dismiss and makes it persistent unless it has been closed before. 'fade', fades out the notification after a set time. 'error' makes this an error notification.
	 * @return	void
	 */
	public function add_notification( $message, $args='' )
	{
		$this->notifications[] = array(
			'message' => $message,
			'args'    => $args
		);
	}

	/**
	 * Show all notifications in the notification area
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	string|null If there are notifications to show, this will return an HTML collection of them.
	 */
	public function get_notifications()
	{
		$notifications = array_merge( $this->notifications, $this->query_notifications() );

		$output = '<div class="' . self::$prefix . '">';
		foreach ( $notifications as $notification )
		{
			$output .= $this->get_notification( $notification['message'], $notification['args'] );
		}
		$output .= '</div>';

		return $output;
	}

	/**
	 * Get and return a notification
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @param	string $message Message for notification
	 * @param	array $args Arguments for notification
	 * @return	string|null If there are notifications to show, this will return an HTML collection of them.
	 */
	public function get_notification( $message, $args )
	{
		$defaults = array(
			'dismiss' => false,
			'event'   => '',
			'fade'    => false,
			'error'   => false,
			'page'    => false
		);
		$args = wp_parse_args( $args, $defaults );
		extract( $args, EXTR_SKIP );

		if ( $page )
		{
			if ( false !== strpos($page, ',') ) $page = explode(',', $page);
			if (! is_page($page) ) return;
		}

		$output = $dismissed = $output_dismiss = '';

		// Set up the dismiss if it is needed
		if ( $dismiss )
		{
			// If event, test if the event has already been dismissed
			$event_data = '';
			if ( $event )
			{
				$current_user = wp_get_current_user();
				$dismissed    = get_user_meta( $current_user->ID, self::$prefix . '_' . $event . '_dismissed', true );
				$event_data   = ' data-event="'. $event .'"';
			}

			$output_dismiss .= ' <a class="' . self::$prefix . '-dismiss"' . $event_data . ' href="#dismiss" title="' . __("Dismiss this message", self::$prefix) . '">';
				$output_dismiss .= __("close", self::$prefix);
			$output_dismiss .= '</a>';
			$output_dismiss  = apply_filters( self::$prefix . '/dismiss', $output_dismiss, $args );
		}

		if (! $dismissed )
		{
			if ( 1 == $fade ) $fade = 'true';

			// Set up the notification
			$output  = '<p' . ($error ? ' class="' . self::$prefix . '-error"' : '') . ($fade ? ' data-fade="' . $fade . '"' : '') . '>';
			$output .= $message;
			$output .= $output_dismiss;
			$output .= '</p>';
		}

		return $output;
	}

	/**
	 * Echo all notifications
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public function show_notifications()
	{
		echo $this->get_notifications();
	}

	/**
	 * Enqueue scripts and styles
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function enqueue_scripts()
	{
		// register acf scripts
		wp_register_script( self::$prefix, plugins_url( 'js/scripts.js', __FILE__ ), array( 'jquery' ), self::$version );
		$args = array(
			'url'    => admin_url( 'admin-ajax.php' ),
			'action' => self::$prefix . '_dismiss',
			'nonce'  => wp_create_nonce( self::$prefix . '_dismiss' )
		);
		wp_localize_script( self::$prefix, 'frontnotify', $args );

		wp_enqueue_script( array(
			self::$prefix
		) );

		// styles
		wp_register_style( self::$prefix, plugins_url( 'css/style.css', __FILE__ ), array(), self::$version );

		wp_enqueue_style( array(
			self::$prefix
		) );
	}
}

endif;