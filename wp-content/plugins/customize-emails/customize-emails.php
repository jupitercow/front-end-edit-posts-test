<?php
/*
Plugin Name: Customize Wordpress Emails
Plugin URI: http://bitbucket.org/jupitercow/customize-emails
Description: At the heart of it, this is meant to update emails to be site-specific instead of Wordpress-specific, but it also updates them to nicer looking, HTML format.
Version: 0.1
Author: Jake Snyder
Author URI: http://Jupitercow.com/
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

------------------------------------------------------------------------
Copyright 2013 Jupitercow, Inc.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

if (! class_exists('customize_emails') ) :

add_action( 'init', array('customize_emails', 'init') );

class customize_emails
{
	/**
	 * Class prefix
	 *
	 * @since 	0.1
	 * @var 	string
	 */
	public static $prefix = __CLASS__;

	/**
	 * Current version of plugin
	 *
	 * @since 	0.1
	 * @var 	string
	 */
	public static $version = '0.1';

	/**
	 * Initialize the Class
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function init()
	{
		add_action( 'admin_enqueue_scripts', array(__CLASS__, 'admin_enqueue_scripts'), 999 );

		// Change the from info to site-specific instead of being from "Wordpress"
		add_filter( 'wp_mail_from_name',     array(__CLASS__, 'wp_mail_from_name') );
		add_filter( 'wp_mail_from',          array(__CLASS__, 'wp_mail_from') );

		// Make all emails html emails
		add_filter( 'wp_mail_content_type',  array(__CLASS__, 'wp_mail_content_type'), 100 );

		// Add new HTML template
		add_action( 'phpmailer_init',        array(__CLASS__, 'add_html_template') );

		// Add email test panel
		add_action( 'admin_menu',            array(__CLASS__, 'admin_menu') );
		// Send preview email through ajax
		add_action( 'wp_ajax_send_preview',  array(__CLASS__, 'send_preview') );
	}

	/**
	 * Replace the email address that emails are sent from. Default: admin_email option.
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function wp_mail_from( $from_email )
	{
		return apply_filters( self::$prefix . '/from/email', get_option('admin_email') );
	}

	/**
	 * Replaces name used that emails will be sent from. Default: blogname option.
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function wp_mail_from_name( $from_name )
	{
		return apply_filters( self::$prefix . '/from/name', get_option('blogname') );
	}

	/**
	 * Switch to HTML emails by default, so we can use our new template
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @param	string $content_type The current content type being used.
	 * @return	string $content_type Modified content type to html or left plaintext depending on filter input.
	 */
	public static function wp_mail_content_type( $content_type )
	{
		if ( apply_filters( self::$prefix . '/use_html_template', true ) )
		{
			return apply_filters( self::$prefix . '/content_type', 'text/html' );
		}

		return $content_type;
	}

	/**
	 * Add new HTML template
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function add_html_template( $phpmailer )
	{
		if (! apply_filters( self::$prefix . '/use_html_template', true ) ) return;

		/**
		 * Plain Text
		 */
		$phpmailer->AltBody = wp_specialchars_decode( $phpmailer->Body, ENT_QUOTES );
		// Add template to message
		$phpmailer->AltBody = self::get_email_template( $phpmailer->AltBody, 'plaintext_template' );
		// Replace variables in email
		$phpmailer->AltBody = self::replace_template_vars( $phpmailer->AltBody );

		/**
		 * HTML Template
		 */
		$phpmailer->Body = self::clean_textlinks( $phpmailer->Body );
		// Convert line breaks & make links clickable
		$phpmailer->Body = nl2br( make_clickable( $phpmailer->Body ) );
		// Add template to message
		$phpmailer->Body = self::get_email_template( $phpmailer->Body );
		// Replace variables in email
		$phpmailer->Body = self::replace_template_vars( $phpmailer->Body );
	}

	/**
	 * Get the email template and add the content.
	 *
	 * Can be overrided by copying templates to theme folder and naming the folder after this class ("customize_emails").
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @param 	string $content The email content
	 * @param 	string $type html/plain, default is html
	 * @return 	string $email The template with content added
	 */
	public static function get_email_template( $content, $type='html' )
	{
		$template_filename = 'html.tpl';
		if ( 'plain' == $type ) $template_filename = 'plain.tpl';

		$template_file_uri = self::locate_template_file( $template_filename );
		$template = file_get_contents($template_file_uri);

		return str_replace( '%content%', $content, $template );
	}

	/**
	 * Load the correct email template file
	 *
	 * Looks in the theme folder first, then loads from the plugin if no theme-override. Can load plaintext or HTML, HTML by default.
	 *
	 * @author  Jake Snyder
	 * @since 	0.1
	 * @param 	string $template The name of the template file
	 * @return 	string $file The location of the file to load
	 */
	public static function locate_template_file( $template_filename )
	{
		// Check theme folder first
		if ( $theme_file_uri = locate_template( array( self::$prefix . '/' . $template_filename ) ) )
		{
			$template_file_uri = $theme_file_uri;
		}
		// Then load from plugin
		else
		{
			$template_file_uri = plugins_url( 'templates/' . $template_filename, __FILE__ );
		}
		return apply_filters( self::$prefix . '/template/' . $template_filename, $template_file_uri );
	}

	/**
	 * Replace variables in the template
	 *
	 * @author  Jake Snyder
	 * @since 	0.1
	 * @param 	string $template Template text
	 * @return 	string Template text with variables replaced
	 */
	public static function replace_template_vars( $template )
	{
		$tags = array(
			'siteurl'     => get_option('siteurl'),
			'blogname'    => apply_filters( self::$prefix . '/from/name', get_option('blogname') ),
			'admin_email' => apply_filters( self::$prefix . '/from/email', get_option('admin_email') ),
			'date'        => date_i18n( get_option('date_format') ),
			'time'        => date_i18n( get_option('time_format') )
		);
		$tags['footer'] = sprintf( __('For more information, please visit <a href="%1$s">%1$s</a><br />Or contact <a href="mailto:%2$s">%2$s</a>'), $tags['siteurl'], $tags['admin_email'] );
		$tags = apply_filters( self::$prefix . '/template/tags', $tags );

		$replace = $replace_with = array();
		foreach ( $tags as $tag => $value )
		{
			$replace[] = "%$tag%";
			$replace_with[] = $value;
		}

		return str_replace($replace, $replace_with, $template);
	}

	/**
	 * Replaces the < & > of the 3.1 email text links
	 *
	 * @since	0.1
	 * @param	string $content Content of email
	 * @return	string Content of email with link cleaned up
	 */
	public static function clean_textlinks( $content )
	{
		return preg_replace( '#<(https?://[^*]+)>#', '$1', $content );
	}

	/**
	 * Add admin settings page to allow you to test email template
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return 	void
	 */
	public static function admin_menu()
	{
		add_options_page(
			__("Email Tests", self::$prefix),
			__("Email Tests", self::$prefix),
			'manage_options',
			self::$prefix . '_options',
			array(__CLASS__, 'admin_page')
		);
	}

	/**
	 * Build admin page
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return 	void
	 */
	public static function admin_page()
	{
		?>
		<div class="wrap">
			<h2><?php _e("Email settings", self::$prefix); ?></h2>

			<h3><?php _e("Send Preview", self::$prefix); ?></h3>

			<p class="instructions <?php echo self::$prefix; ?>_instructions"></p>

			<div id="message"></div>
			<table class="form-table">
				<tr valign="top">

					<th scope="row">
						<label for="<?php echo self::$prefix; ?>_email"><?php _e('Send a preview email to&hellip;', self::$prefix); ?></label>
					</th>

					<td>
						<input type="text" id="<?php echo self::$prefix; ?>_email" class="regular-text" value="<?php esc_attr_e( get_option('admin_email') ); ?>" />
						<select id="<?php echo self::$prefix; ?>_type">
							<option value="registration">Registration</option>
							<option value="registration_admin">Registration, Admin</option>
							<option value="lost_password">Lost Password</option>
						</select>
						<a href="#send_preview" class="button" id="<?php echo self::$prefix; ?>_send_preview"><?php _e("Send", self::$prefix); ?></a>
					</td>

				</tr>
			</table>

		</div>
		<?php
	}

	/**
	 * Load javascript for email preview
	 *
	 * @author  Jake Snyder
	 * @since 	0.1
	 * return	void
	 */
	public static function admin_enqueue_scripts( $hook )
	{
		if ( 'settings_page_customize_emails_options' != $hook ) return;

		// register acf scripts
		wp_register_script( self::$prefix, plugins_url( 'js/scripts.js', __FILE__ ), array('jquery'), self::$version );
		$args = array(
			'url'     => admin_url( 'admin-ajax.php' ),
			'action'  => 'send_preview',
			'prefix'  => self::$prefix,
			'version' => self::$version,
			'nonce'   => wp_create_nonce( self::$prefix . '/nonce/email_preview' ),
			'spinner' => admin_url( 'images/loading.gif' )
		);
		wp_localize_script( self::$prefix, 'email_preview', $args );
		wp_enqueue_script( self::$prefix );
	}

	/**
	 * Send an email preview to test the template
	 *
	 * These email messages are just copied from wp-login.php and "wp_new_user_notification()" in wp-includes/pluggable.php, if they change there, they will need to be recopied.
	 *
	 * @author  Jake Snyder
	 * @since 	0.1
	 * @return	void
	 */
	public static function send_preview()
	{
		if (! current_user_can( 'manage_options' ) ) die();

   		// vars
		$options = array(
			'email' => '',
			'type' => 'registration',
			'nonce' => ''
		);

		// load post options
		$options = array_merge($options, $_POST);

		// test options
		foreach ( $options as $key => $option )
		{
			if (! $option ) die('missing option: '. $key);#
		}

		// verify nonce
		if( ! wp_verify_nonce($options['nonce'], self::$prefix . '/nonce/email_preview') )
		{
			die('Refresh.');
		}

		$email = sanitize_email( $options['email'] );

		if (! is_email($email) )
		{
			die( __("Please enter a valid email", self::$prefix) );
		}

		$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

		// From wp-login.php
		if ( 'lost_password' == $options['type'] )
		{
			$title = sprintf( __('[%s] Password Reset'), $blogname );

			$message  = __('Someone requested that the password be reset for the following account:') . "\r\n\r\n";
			$message .= network_home_url( '/' ) . "\r\n\r\n";
			$message .= sprintf(__('Username: %s'), '%username%') . "\r\n\r\n";
			$message .= __('If this was a mistake, just ignore this email and nothing will happen.') . "\r\n\r\n";
			$message .= __('To reset your password, visit the following address:') . "\r\n\r\n";
			$message .= '<' . network_site_url("wp-login.php?action=rp&key=%key%&login=%username%", 'login') . ">\r\n";
		}
		// From pluggable.php
		elseif ( 'registration_admin' == $options['type'] )
		{
			$title = sprintf(__('[%s] New User Registration'), $blogname);

			$message  = sprintf(__('New user registration on your site %s:'), $blogname) . "\r\n\r\n";
			$message .= sprintf(__('Username: %s'), '%username%') . "\r\n\r\n";
			$message .= sprintf(__('E-mail: %s'), '%email@address.com%') . "\r\n";
		}
		// From pluggable.php
		else
		{
			$title = sprintf(__('[%s] Your username and password'), $blogname);

			$message  = sprintf(__('Username: %s'), '%username%') . "\r\n";
			$message .= sprintf(__('Password: %s'), '%password%') . "\r\n";
			$message .= wp_login_url() . "\r\n";
		}

		// Send the preview email
		if ( wp_mail($email, $title, $message) )
		{
			die( sprintf( __("An email preview has been successfully sent to %s", self::$prefix), $email ) );
		}
		else
		{
			die( __("An error occured while sending email. Please check your server configuration.", self::$prefix) );
		}
	}
}

endif;