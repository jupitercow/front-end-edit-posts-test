# Simplify Backend Profiles

Wordpress plugin that removes a lot of the stuff on user profiles that never really gets used and just makes the interface confusing for new users.