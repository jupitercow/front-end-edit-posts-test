<?php
/*
Plugin Name: Advanced Custom Fields: Notify Admin
Plugin URI: http://bitbucket.org/jupitercow/acf-notify-admin
Description: Allows a form to notify the adminitstrator or a set list of email addresses when submitted.
Version: 0.1
Author: Jake Snyder
Author URI: http://Jupitercow.com/
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

------------------------------------------------------------------------
Copyright 2013 Jupitercow, Inc.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

if (! class_exists('acf_notify_admin') ) :

add_action( 'init', array('acf_notify_admin', 'init') );

class acf_notify_admin
{
	/**
	 * The default status for new posts
	 *
	 * @since	0.1
	 * @var 	string
	 */
	public static $prefix = 'notify_admin';

	/**
	 * Current version of plugin
	 *
	 * @since	0.1
	 * @var 	string
	 */
	public static $version = '0.1';

	/**
	 * Initialize the Class
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public function init()
	{
		if (! self::test_requirements() ) return;

		// Add action to email the admin when a form has been submitted
		add_action('acf/save_post', array(__CLASS__, 'notify'), 999 );
	}

	/**
	 * Make sure that any neccessary dependancies exist
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	bool True if everything exists
	 */
	public static function test_requirements()
	{
		// Look for ACF
		if (! class_exists('Acf') ) return false;
		return true;
	}

	/**
	 * Send emails to the admin
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function notify( $post_id )
	{
		// get the post
		$post = get_post($post_id);
		$post_type = $post->post_type;

		// Make sure this isn't an ACF post update
		if ( 'acf' == $post_type ) return;

		// Make sure this is not in the admin, unless allowed for some reason...
		if ( (is_admin() && apply_filters( 'acf/' . self::$prefix . '/allow_admin', false )) || ! is_admin() )
		{
			// Test if notifications are on/off globally or on/off for post_type.
			if (! apply_filters( 'acf/' . self::$prefix . "/on/type={$post_type}", apply_filters( 'acf/' . self::$prefix . "/on", true ) ) ) return;

			// get the current user
			$current_user = wp_get_current_user();

			$fields = (! empty($_POST['fields']) ) ? $_POST['fields'] : array();

			if ( is_object($post) && $fields )
			{
				$admin_emails = array( get_option('admin_email') );
				$admin_emails = apply_filters( 'acf/' . self::$prefix . "/email/addresses/type={$post_type}", apply_filters( 'acf/' . self::$prefix . '/email/addresses', $admin_emails ) );

				$full_name = $current_user->first_name . ' ' . $current_user->last_name;
				$subject  = get_option('blogname') . ': Post Edited By ' . esc_html($full_name);
				$subject  = apply_filters( 'acf/' . self::$prefix . "/email/subject/type={$post_type}", apply_filters( 'acf/' . self::$prefix . '/email/subject', $subject, $post_id ) );

				$message  = 'A post was recently edited on ' . home_url() . ".\n\n";
				$message .= 'Date: ' . date_i18n(get_option('date_format')) . ', ' . date_i18n(get_option('time_format')) . ".\n\n";
				$message .= 'Post title: ' . get_the_title($post_id) . ".\n\n";
				$message .= "User who edited:\n";
					$message .= "\t Name: " . $full_name . ".\n";
					$message .= "\t Email: " . $current_user->user_login . ".\n";
					$message .= "\t Username: " . $current_user->user_email . ".\n\n";
				$message .= 'View post: ' . get_permalink($post_id) . ".\n";
				$message .= 'Edit post: ' . self::get_edit_post_link($post_id, '') . ".\n\n";
				$message  = apply_filters( 'acf/' . self::$prefix . "/email/message/type={$post_type}", apply_filters( 'acf/' . self::$prefix . '/email/message', $message, $post_id ), $post_id );

				if ( is_array($admin_emails) )
				{
					foreach ( $admin_emails as $to )
					{
						wp_mail( $to, $subject, $message );
					}
				}
				elseif ( is_string($admin_emails) )
				{
					wp_mail( $admin_emails, $subject, $message );
				}
			}
		}
	}

	/**
	 * get_edit_post_link even when current user doesn't officially have access to edit post.
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	string The link.
	 */
	public static function get_edit_post_link( $id = 0, $context = 'display' )
	{
		if (! $post = get_post( $id ) ) return;

		if ( 'revision' === $post->post_type )
			$action = '';
		elseif ( 'display' == $context )
			$action = '&amp;action=edit';
		else
			$action = '&action=edit';
	
		$post_type_object = get_post_type_object( $post->post_type );
		if (!$post_type_object )
			return;

		return apply_filters( 'get_edit_post_link', admin_url( sprintf($post_type_object->_edit_link . $action, $post->ID) ), $post->ID, $context );
	}
}

endif;